exports.computeDistance = (location1, location2) => {
	const R = 6371;
	var dLat = deg2rad(location2.lat - location1.lat);
	var dLon = deg2rad(location2.lng - location1.lng);
	var a =
	Math.sin(dLat/2) * Math.sin(dLat/2) +
	Math.cos(deg2rad(location1.lat)) * Math.cos(deg2rad(location2.lat)) *
	Math.sin(dLon/2) * Math.sin(dLon/2)
	;
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	var d = R * c;
	return d;
}

exports.random = (from, to) => {
	return Math.floor(Math.random() * (to - from + 1)) + from;
}

function deg2rad(deg) {
	return deg * (Math.PI/180)
}

exports.getToken = (cookie, name) => {
  var cookiestring = RegExp(""+name+"[^;]+").exec(cookie);
  return decodeURIComponent(!!cookiestring ? cookiestring.toString().replace(/^[^=]+./,"") : "");
}
