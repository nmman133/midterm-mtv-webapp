var uniqid = require('uniqid');
var moment = require('moment');
var constant = require('./constants');

var requests = [];
var mapping = new Map();

exports.add = (info) => {
	var id = uniqid();
	var now = moment();
	var req = {
		id: id,
		status: constant.STATUS_NEW,
		time: {
			value: now.valueOf(),
			display: now.format('h:mm:ss a | DD-MM-YYYY')
		},
		info: info,
		attempt: 0,
		location: {
			lat: -1,
			lng: -1
		},
		driver: {
			username: '',
			name: '-',
			phone: '-',
			location: {
				lat: -1,
				lng: -1
			}
		}
	};
	requests.unshift(req);
	mapping.set(id, req);
}

exports.all = () => {
	return requests;
}

exports.at = (id) => {
	return mapping.get(id);
}

exports.each = (apply) => {
	for (i in requests) {
		apply(requests[i]);
	}
}
