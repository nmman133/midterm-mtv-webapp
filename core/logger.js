var jwt = require('jsonwebtoken');
var config = require('../config/config');
var rndToken = require('rand-token');
var sqlite = require('../db/sqlite');
var constant = require('./constants');
var moment = require('moment');
var Cookies = require('cookies');

exports.generateAccessToken = function (payload) {
    var token = jwt.sign(payload, config.accessTokenSecret, {
        expiresIn: config.accessTokenExpiresIn
    });
    console.log('Generate new access token');
    return token;
};

exports.generateRefreshToken = function () {
    var SIZE = 80;
    var token = rndToken.generate(SIZE);
    console.log('Generate new refresh token');
    return token;
};

// check token
exports.verifyAccessToken = function (req, res, next) {
    console.log('Verifying access token...');
    // Get cookies
    var cookies = new Cookies(req, res);
    var accessToken = cookies.get('ACCESS_TOKEN');
    var refreshToken = cookies.get('REFRESH_TOKEN');

    if (!accessToken) {
        serverReject(res, 403, "NO_TOKEN");
    } else {
        jwt.verify(accessToken, config.accessTokenSecret, function (err, payload) {
            if (err) {
                if (err.name == 'TokenExpiredError') {
                    var decoded = jwt.decode(accessToken);
                    sqlite.getToken(decoded.USERNAME).then(function (data) {
                        if (data[0].TOKEN != refreshToken) {
                            serverReject(res, 403, "INVALID_ACCESS_TOKEN");
                        } else if (moment().unix() - parseInt(data[0].DATE) >= config.refreshTokenExpiresIn) {
                            serverReject(res, 403, "REFRESH_TOKEN_EXPRIED");
                        } else {
                            exports.renewToken(decoded, req, res, next);
                        }
                    }).catch(function (err) {
                        serverReject(res, 500, "SYS_ERROR");
                    });
                } else {
                    serverReject(res, 403, "INVALID_ACCESS_TOKEN");
                }
            } else {
                console.log('Success');
                next();
            }
        });
    }
};

exports.verifyAccessTokenForSocket = function (accessToken, refreshToken, resolve, reject) {
    console.log('Verifying access token for socket connection...');
    jwt.verify(accessToken, config.accessTokenSecret, function (err) {
        var decoded = jwt.decode(accessToken);
        if (err) {
            console.log('Fail');
            reject();
        } else {
            console.log('Success');
            resolve(decoded);
        }
    });
}

exports.renewToken = function (payload, req, res, next) {
    console.log('New token');
    delete payload.exp;
    delete payload.iat;
    var accesstoken = exports.generateAccessToken(payload);
    var refreshtoken = exports.generateRefreshToken();
    var cookies = new Cookies(req, res);
    cookies.set('ACCESS_TOKEN', accesstoken, { expires: new Date('9999/9/9'), httpOnly: true });
    cookies.set('REFRESH_TOKEN', refreshtoken, { expires: new Date('9999/9/9'), httpOnly: true });
    sqlite.updateToken(payload.USERNAME, refreshtoken).then(function (data) {
        next();
    }).catch(function (err) {
        serverReject(res, 500, "SYS_ERROR");
    });
}

function serverReject(res, code, msg) {
    res.status(code).send(msg);
}
