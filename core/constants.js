// Roles
exports.ROLE_IDENTIFIER = 0;
exports.ROLE_MANAGER = 1;
exports.ROLE_DRIVER = 2;
exports.ROLE_SUBMITTER = 3;

// Request status
exports.STATUS_NEW = 0;
exports.STATUS_IDENTIFIED = 1;
exports.STATUS_ASSIGNED = 2;
exports.STATUS_MOVING = 3;
exports.STATUS_COMPLETED = 4;
exports.STATUS_FAIL = 5;
exports.STATUS_PROCESSING = 11;

// String
exports.GROUP_MANAGER = 'managers';
