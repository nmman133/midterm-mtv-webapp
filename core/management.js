var uniqid = require('uniqid');
var constant = require('./constants');
var requests = require('./request-queue');
var helper = require('./helper');
var logger = require('./logger');
var Cookies = require('cookies');

var io;

var socketMap = new Map();
var driverAssignedMap = new Map();
var identifierAssignedSet = new Set();

const MAX_ATTEMPT = 3;

var identifierSockets = [];
var driverSockets = [];

exports.newRequest = (info) => {
    requests.add(info);
    updateManagers();
    assignToIdentifiers();
}

// Setup socketio
exports.start = (_io) => {
    io = _io;

    // Init middleware for authentication
    initMidleware(io);

    // Set listener for socket event
    io.sockets.on('connection', (socket) => {

        // When location-identifier submit
        socket.on('identifier/submit', (data, callback) => {
            if (data) {
                callback();
                onIdentified(data);
            }
        });

        // When driver update his status
        socket.on('driver/update-status', (status) => {
            if (status != undefined) {
                socketMap.get(socket.id).status = status;
                console.log("Driver update status: " + status);
                if (status == 1) {
                    requests.each(req => {
                        if (req.status == constant.STATUS_IDENTIFIED || req.status == constant.STATUS_FAIL) {
                            findDriver(req);
                        }
                    });
                }
            }
        });

        socket.on('driver/accept', (reqId) => {
            if (reqId) {
                onRequestAccept(reqId, socket.id);
            }
        });

        socket.on('driver/reject', (reqId) => {
            if (reqId) {
                onRequestReject(reqId, socket.id);
            }
        });

        socket.on('driver/begin', (reqId) => {
            if (reqId) {
                onRequestBegin(reqId);
            }
        });

        socket.on('driver/end', (reqId) => {
            if (reqId) {
                onRequestEnd(reqId, socket.id);
            }
        });

        // When driver update his location
        socket.on('driver/update-location', (lat, lng) => {
            if (lat != undefined && lng != undefined) {
                var metadata = socketMap.get(socket.id);
                metadata.data.location.lat = lat;
                metadata.data.location.lng = lng;
                var reqId = metadata.req;
                if (reqId) {
                    requests.at(reqId).driver.location.lat = lat;
                    requests.at(reqId).driver.location.lng = lng;
                }
                console.log("Driver update location: " + lat.toString() + " | " + lng.toString());
                updateManagers();
            }
        });

        // When someone disconnect with server
        socket.on('disconnect', () => {
            var sub = socketMap.get(socket.id);
            if (sub) {
                if (sub.role == constant.ROLE_IDENTIFIER) {
                    sub.mapping.forEach(element => {
                        identifierAssignedSet.delete(element);
                    });
                }
                if (sub.role == constant.ROLE_DRIVER && sub.req) {
                    requests.at(sub.req).status = constant.STATUS_FAIL;
                }
                if (sub.role != constant.ROLE_MANAGER) {
                    getListByRole(sub.role).splice(sub.pos, 1);
                    socketMap.delete(socket.id);
                }
                console.log('client ' + socket.id + ' disconnected | role: ' + sub.role.toString());
            }
        });
    });
}

// Init socketio middleware
function initMidleware(io) {
    io.use((socket, next) => {
        var cookie = socket.request.headers.cookie;
        var accessToken;
        var refreshToken;

        if (cookie) {
            accessToken = helper.getToken(cookie, "ACCESS_TOKEN");
            refreshToken = helper.getToken(cookie, "REFRESH_TOKEN");
        } else {
            accessToken = socket.handshake.query.access_token;
            refreshToken = socket.handshake.query.refresh_token;
        }

        logger.verifyAccessTokenForSocket(accessToken, refreshToken, (payload) => {
            if (initClient(socket, payload)) {
                console.log('socket ' + socket.id + ' connected | role: ' + payload.ROLE.toString());
                next();
            } else {
                socket.disconnect();
                next();
            }
        }, () => {
            socket.disconnect();
            next();
        });
    });
}

function initClient(socket, payload) {
    var metadata;
    var role = payload.ROLE;
    switch (role) {
        case constant.ROLE_MANAGER:
            socket.join(constant.GROUP_MANAGER);
            metadata = {
                role: role
            };
            socketMap.set(socket.id, metadata);
            socket.emit('manager/update', requests.all());
            break;
        case constant.ROLE_IDENTIFIER:
            metadata = {
                role: role,
                pos: identifierSockets.length,
                mapping: []
            };
            socketMap.set(socket.id, metadata);
            identifierSockets.push(socket);
            assignToIdentifiers();
            break;
        case constant.ROLE_DRIVER:
            metadata = {
                role: role,
                pos: driverSockets.length,
                status: 0,
                inProcessing: 0,
                data: {
                    username: payload.USERNAME,
                    name: payload.NAME,
                    phone: payload.PHONE,
                    location: {
                        lat: -1,
                        lng: -1
                    }
                },
                req: undefined
            }
            socketMap.set(socket.id, metadata)
            driverSockets.push(socket);
            break;
        default:
            return false;
    }
    return true;
}

function getListByRole(role) {
    switch (role) {
        case constant.ROLE_IDENTIFIER:
            return identifierSockets;
        case constant.ROLE_DRIVER:
            return driverSockets;
    }
}

function assignToIdentifiers() {
    requests.each((req) => {
        if (req.status == constant.STATUS_NEW && !identifierAssignedSet.has(req.id) && identifierSockets.length > 0) {
            var id = helper.random(0, identifierSockets.length - 1);
            var socket = identifierSockets[id];
            socketMap.get(socket.id).mapping.push(req.id);
            socket.emit('identifier/new-request', {
                id: req.id,
                info: req.info
            });
            identifierAssignedSet.add(req.id);
        }
    });
}

function onIdentified(data) {
    var req = requests.at(data.id);
    req.status = constant.STATUS_IDENTIFIED;
    req.location = data.location;
    identifierAssignedSet.delete(req.id);
    updateManagers();
    findDriver(req);
}

function updateManagers() {
    if (io) {
        io.sockets.in(constant.GROUP_MANAGER).emit('manager/update', requests.all());
    }
}

function findDriver(req) {
    var minDis = Number.MAX_SAFE_INTEGER;
    var socket = undefined;
    for (i in driverSockets) {
        var tempSocket = driverSockets[i];
        var metadata = socketMap.get(tempSocket.id);
        if (!metadata.inProcessing && metadata.status == 1 && metadata.data.location.lat != -1 && !isRejectedDriver(req.id, tempSocket.id)) {
            let dis = helper.computeDistance(req.location, metadata.data.location);
            console.log('distance: ' + dis);
            if (dis < minDis) {
                socket = tempSocket;
                minDis = dis;
            }
        }
    }
    if (socket) {
        socketMap.get(socket.id).inProcessing = 1;
        req.status = constant.STATUS_PROCESSING;
        socket.emit('driver/new-request', req.id, req.info.name, req.info.phone, req.info.address, req.info.note,
            req.location.lat, req.location.lng);
        req.attempt += 1;
    } else {
        req.status = constant.STATUS_FAIL;
        updateManagers();
    }
}

function isRejectedDriver(reqId, socketId) {
    if (driverAssignedMap.has(reqId)) {
        var ls = driverAssignedMap.get(reqId);
        for (i in ls) {
            if (ls[i] === socketId) {
                return true;
            }
        }
    }
    return false;
}

function onRequestReject(reqId, socketId) {
    socketMap.get(socketId).inProcessing = 0;
    var req = requests.at(reqId);
    if (req.attempt >= MAX_ATTEMPT) {
        req.status = constant.STATUS_FAIL;
        req.driver = {};
        updateManagers();
    } else {
        if (driverAssignedMap.has(reqId)) {
            driverAssignedMap.get(reqId).push(socketId);
        } else {
            driverAssignedMap.set(reqId, [socketId]);
        }
        findDriver(req);
    }
}

function onRequestAccept(reqId, socketId) {
    var req = requests.at(reqId);
    req.status = constant.STATUS_ASSIGNED;
    req.driver = socketMap.get(socketId).data;
    socketMap.get(socketId).req = reqId;
    updateManagers();
}

function onRequestBegin(reqId) {
    var req = requests.at(reqId);
    req.status = constant.STATUS_MOVING;
    updateManagers();
}

function onRequestEnd(reqId, socketId) {
    socketMap.get(socketId).inProcessing = 0;
    var req = requests.at(reqId);
    req.status = constant.STATUS_COMPLETED;
    socketMap.get(socketId).req = undefined;
    driverAssignedMap.delete(reqId);
    updateManagers();
}