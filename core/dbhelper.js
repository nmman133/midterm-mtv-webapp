const mysql = require('mysql');

var mysqlConnection = mysql.createConnection({
	host:'localhost',
	user:'root',
	password:'anhvu1512',
	database:'PICKME',
	multipleStatements: true
});

mysqlConnection.connect((err) => {
	if(!err)
		console.log('DB connection succeeded.');
	else
		console.log('DB connection failed \n Error : ' + JSON.stringify(err, undefined, 2));
});

// get all drivers
exports.getAllDriver = () => {
	return new Promise((resolve, reject) => {
        mysqlConnection.query('SELECT * FROM DRIVER',(err, rows, fields)=>{
			if(!err)
				resolve(rows);
			else
				reject(err);
		})
    })
};

// get a driver
exports.getDriver = (id) => {
	return new Promise((resolve, reject) => {
		mysqlConnection.query('SELECT * FROM DRIVER WHERE id = ?',[id],(err, rows, fields)=>{
			if(!err)
				resolve(rows);
			else
				reject(err);
		})
	})
};

// delete a driver
exports.deleteDriver = (id) => {
	return new Promise((resolve, reject) => {
		mysqlConnection.query('DELETE FROM DRIVER WHERE id = ?',[id],(err, rows, fields)=>{
			if(!err)
				resolve('Deleted successfully.');
			else
				reject(err);
		})
	})
};

// insert an driver
exports.insertDriver = (id, username, password) => {
	return new Promise((resolve, reject) => {
		var sql = "SET @id=?; SET @username=?; SET @password=?;\
		CALL DriverAddOrEdit(@id,@username,@password);"
		mysqlConnection.query(sql,[id, username, password],(err, rows, fields)=>{
			if(!err)
				rows.forEach(element => {
					if(element.constructor == Array)
						resolve('Inserted driver id: ' + element[0].id);
				});
			else
				reject(err);
		})
	})
}

// update a driver
exports.updateDriver = (id, username, password) => {
	return new Promise((resolve, reject) => {
		var sql = "SET @id=?; SET @username=?; SET @password=?; \
		CALL DriverAddOrEdit(@id,@username,@password);"
		mysqlConnection.query(sql,[id, username, password],(err, rows, fields)=>{
			if(!err)
				resolve('Updated successfully');
			else
				reject(err);
		})
	})
};