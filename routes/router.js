var router = require('express').Router();
var path = require('path');
var manager = require('../core/management');
var logger = require('../core/logger');
var sqlite = require('../db/sqlite');
var constant = require('../core/constants');
var Cookies = require('cookies');

router.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '../views/login.html'));
});

router.get('/home', logger.verifyAccessToken, (req, res) => {
    res.sendFile(path.join(__dirname, '../views/home.html'));
});

router.get('/identifier', logger.verifyAccessToken, (req, res) => {
    res.sendFile(path.join(__dirname, '../views/location-identifier.html'));
});

router.get('/manager', logger.verifyAccessToken, (req, res) => {
    res.sendFile(path.join(__dirname, '../views/manager.html'));
});

router.post('/logon', (req, res) => {
    sqlite.getUser(req.body.username, req.body.password).then(function (data) {
        if (typeof data !== 'undefined' && data.length > 0) {
            var payload = data[0];
            delete payload.exp;
            delete payload.iat;
            var accesstoken = logger.generateAccessToken(payload);
            var refreshtoken = logger.generateRefreshToken();
            var response = {
                access_token: accesstoken,
                refresh_token: refreshtoken,
                username: payload.USERNAME,
                name: payload.NAME,
                phone: payload.PHONE
            }
            if (payload.ROLE != constant.ROLE_DRIVER) {
                if (payload.ROLE == constant.ROLE_SUBMITTER) {
                    response.next_url = '/home';
                } else if (payload.ROLE == constant.ROLE_IDENTIFIER) {
                    response.next_url = '/identifier';
                } else if (payload.ROLE = constant.ROLE_MANAGER) {
                    response.next_url = '/manager';
                }
            }
            var cookies = new Cookies(req, res);
            cookies.set('ACCESS_TOKEN', accesstoken, { expires: new Date('9999/9/9'), httpOnly: true });
            cookies.set('REFRESH_TOKEN', refreshtoken, { expires: new Date('9999/9/9'), httpOnly: true });
            sqlite.updateToken(payload.USERNAME, refreshtoken).then(function (data) {
                res.statusCode = 200;
                res.json(response);
            }).catch(function (err) {
                res.status(500).send('Lỗi server');
            });
        } else {
            res.status(403).send('Nhập sai username hoặc password');
        }
    }).catch(function (err) {
        res.status(500).send('Lỗi server');
    });
});

router.post('/logout', function(req, res) {
    var username = req.body.username;
    console.log(username);
    if (username) {
        sqlite.deleteToken(username);
        res.clearCookie('ACCESS_TOKEN');
        res.clearCookie('REFRESH_TOKEN');
        res.end();
    }
});

router.get('/socket', function(req, res) {
    var cookies = new Cookies(req, res);
    var accessToken = cookies.get('ACCESS_TOKEN');
    var refreshToken = cookies.get('REFRESH_TOKEN');
    if (accessToken && refreshToken) {
        res.json({
            access_token: accessToken,
            refresh_token: refreshToken
        });
    } else {
        res.statusCode = 403;
    }
});

router.get('/submit', logger.verifyAccessToken, (req, res, next) => {
    var name = req.query.name;
    var phone = req.query.phone;
    var address = req.query.address;
    var note = req.query.note;
    var info = {
        name: name,
        phone: phone,
        address: address,
        note: note
    };
    var reqId = manager.newRequest(info);
    res.send(reqId);
});

module.exports = (io) => {
    manager.start(io);
    return router;
};
