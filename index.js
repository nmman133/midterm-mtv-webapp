var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var http = require('http');
var socketIO = require('socket.io');
var morgan = require('morgan');
var cors = require('cors');

const app = express();
const server = http.createServer(app);
const io = socketIO(server);

var router = require('./routes/router')(io);

// parse application/json
app.use(bodyParser.json());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: true}));
// log requests to the console
//app.use(morgan('dev'));

app.use(cors());

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', router);

const PORT = process.env.PORT || 8080;

server.listen(PORT, () => {
	console.log('Server is listening on port 8080');
});
