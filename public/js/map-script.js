const APP_ID = 'OoYwq36RZqBtKmnNkxRI';
const APP_CODE = 'IRJeAJ-nccXK6D2FGl_lPw';
const DEFAULT_ZOOM = 16;

var platform = new H.service.Platform({
    app_id: APP_ID,
    app_code: APP_CODE,
    useHTTPS: true
});

var pixelRatio = window.devicePixelRatio || 1;

var defaultLayers = platform.createDefaultLayers({
    tileSize: pixelRatio === 1 ? 256 : 512,
    ppi: pixelRatio === 1 ? undefined : 320
});

var map = new H.Map(document.getElementById('map'), defaultLayers.normal.map, { pixelRatio: pixelRatio });

var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

var ui = H.ui.UI.createDefault(map, defaultLayers);

var geocoder = platform.getGeocodingService();

var primaryMarker;

var otherMarker = new H.map.Group();

function setPrimaryMarker(lat, lng, zoom) {
    var pos = { lat: lat, lng: lng };
    if (primaryMarker) {
        map.removeObject(primaryMarker);
    }
    primaryMarker = new H.map.Marker(pos);
    primaryMarker.draggable = true;
    map.addObject(primaryMarker);
    map.setCenter(pos);
    map.setZoom(zoom);
}

function setOtherMarker(position) {
    var marker = new H.map.Marker(position);
    otherMarker.addObject(marker);
    map.addObject(otherMarker);
}

function moveMapTo(lat, lng, zoom) {
    map.setCenter({ lat: lat, lng: lng });
    map.setZoom(zoom);
}

function handleSearchRequest(res) {
    results = [];
    for (i in res.suggestions) {
        var item = res.suggestions[i];
        results.push({
            label: item.label.replace(/<(.|\n)*?>/g, '')
        });
    }
    return results;
}

function autocomplete(query, resolve) {
    var url = `http://autocomplete.geocoder.api.here.com/6.2/suggest.json?app_id=${APP_ID}&app_code=${APP_CODE}&query=${query}&beginHighlight=<b>&endHighlight=</b>`;
    $.get(url, function (res) {
        if (res) {
            resolve(handleSearchRequest(res));
        }
    });
}

function geocode(query, resolve, reject) {
    var geocodingParams = {
        searchText: query
    };
    geocoder.geocode(geocodingParams, function (res) {
        var view = res.Response.View;
        if (view && view[0]) {
            var locations = view[0].Result;
            if (locations.length > 0) {
                var lat = locations[0].Location.DisplayPosition.Latitude;
                var lng = locations[0].Location.DisplayPosition.Longitude;
                setPrimaryMarker(lat, lng, DEFAULT_ZOOM);
                resolve(lat, lng);
                return;
            }
        }
        reject('Geocode fail');
    }, function (e) {
        reject(e);
    });
}

function addDraggableMarker(onNewPosition){
    map.addEventListener('dragstart', function(ev) {
      var target = ev.target;
      if (target instanceof H.map.Marker) {
        behavior.disable();
      }
    }, false);

    map.addEventListener('dragend', function(ev) {
      var target = ev.target;
      if (target instanceof mapsjs.map.Marker) {
        behavior.enable();
        onNewPosition(target.getPosition().lat, target.getPosition().lng);
      }
    }, false);

    map.addEventListener('drag', function(ev) {
      var target = ev.target,
          pointer = ev.currentPointer;
      if (target instanceof mapsjs.map.Marker) {
        target.setPosition(map.screenToGeo(pointer.viewportX, pointer.viewportY));
      }
    }, false);
  }

  moveMapTo(10.77822, 106.70323, 13);