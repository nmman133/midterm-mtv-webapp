const APP_ID = 'OoYwq36RZqBtKmnNkxRI';
const APP_CODE = 'IRJeAJ-nccXK6D2FGl_lPw';
const DEFAULT_ZOOM = 15;

var map, driverMarker, userMarker, routeLine, router;

function createMap(driver, user) {
    var platform = new H.service.Platform({
        app_id: APP_ID,
        app_code: APP_CODE,
        useHTTPS: true
    });

    var pixelRatio = window.devicePixelRatio || 1;

    var defaultLayers = platform.createDefaultLayers({
        tileSize: pixelRatio === 1 ? 256 : 512,
        ppi: pixelRatio === 1 ? undefined : 320
    });

    map = new H.Map(document.getElementById('map'), defaultLayers.normal.map, { pixelRatio: pixelRatio })
    router = platform.getRoutingService();
    var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
    var ui = H.ui.UI.createDefault(map, defaultLayers);

    driverMarker = new H.map.Marker(driver);
    userMarker = new H.map.Marker(user);
    map.addObjects([driverMarker, userMarker]);

    map.setCenter(driver);
    map.setZoom(DEFAULT_ZOOM);
    showRouteline(map, router, driver, user);
}

function showRouteline(map, router, from, to) {
    var waypoint0 = 'geo!' + from.lat.toString() + ',' + from.lng.toString();
    var waypoint1 = 'geo!' + to.lat.toString() + ',' + to.lng.toString();
    var routingParameters = {
        'mode': 'fastest;car',
        'waypoint0': waypoint0,
        'waypoint1': waypoint1,
        'representation': 'display'
    };
    router.calculateRoute(routingParameters, function (result) {
        var route,
            routeShape,
            linestring;
        if (result.response.route) {
            route = result.response.route[0];
            routeShape = route.shape;
            linestring = new H.geo.LineString();
            routeShape.forEach(function (point) {
                var parts = point.split(',');
                linestring.pushLatLngAlt(parts[0], parts[1]);
            });
            routeLine = new H.map.Polyline(linestring, {
                style: { strokeColor: 'blue', lineWidth: 5 }
            });
            map.addObject(routeLine);
            map.setViewBounds(routeLine.getBounds());
        }
    }, function (error) {
        alert(error.message);
    });
}

function updateDriver(lat, lng) {
    if (map && driverMarker) {
        map.removeObject(driverMarker);
        driverMarker = new H.map.Marker({
            lat: lat,
            lng: lng
        });
        map.addObject(driverMarker);
    }
}

function removeRoute() {
    if (map && routeLine) {
        map.removeObject(routeLine);
    }
}

function removeUser() {
    if (map && userMarker) {
        map.removeObject(userMarker);
    }
}
