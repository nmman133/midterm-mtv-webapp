var username = sessionStorage.getItem("username");
const socket = io.connect('http://localhost:8080');

socket.on('new-token', function(uname, acToken, reToken) {
    accessToken = acToken;
    refreshToken = reToken;
});

socket.on('connect', function() {
    console.log('connected');
});

socket.on('disconnect', function() {
    window.location.pathname = '/';
});

var requests = new Array();
var currentRequest;

var suggestions = [];
var currentSuggestion;

socket.on('identifier/new-request', function(req) {
    requests.push(req);
    console.log(requests);
    $('#request-number').text(requests.length.toString());
    if (!currentRequest) {
        next();
    }
})

$('#username').text(username);
$(".dropdown-item#logout").on('click', function(event) {
    var param = {
        username: username
    };

    $.post('/logout', param)
        .done(function(result) {
            window.location.pathname = '/';
        })
        .fail(function(xhr, status, error) {
            swal("Oops!!", "Đã có vấn đề gì đó...", "error");

        });
});

function next() {
    if (requests.length < 1) {
        $('#request').html('Không có chuyến xe');
        return;
    }

    var source = $('#requests-template').html();
    var template = Handlebars.compile(source);

    var html = template(requests[0].info);
    currentRequest = requests[0];

    $('#request').html(html);
    $('#locate-address').click(function() {
        locate(currentRequest.info.address);
    });
    $('#locate-suggestion').click(function() {
        if (currentSuggestion) {
            locate(currentSuggestion);
        }
    });
    $('#suggest').click(suggest);
    $("#identifier-form").submit(function(e) {
        e.preventDefault();
        confirm();
    });
}

function locate(query) {
    if (query && query != '') {
        geocode(query, function(lat, lng) {
            $('#lat').attr('value', lat);
            $('#lng').attr('value', lng);
        }, function(e) {
            swal("Oops!!", e, "error");

        });
    }
}

function suggest() {
    var query = currentRequest.info.address;
    autocomplete(query, function(res) {
        if (res.length > 0) {
            suggestions = res;
            var source = $('#suggestion-template').html();
            var template = Handlebars.compile(source);
            var html = template({
                suggestions: suggestions
            });
            $('#dropdown-suggestion').html(html);
            $('.dropdown-item').click(onSuggestionSelected)

            currentSuggestion = suggestions[0].label;
            $('#suggestion').attr('value', currentSuggestion);
        }
    });
}

function onSuggestionSelected() {
    currentSuggestion = $(this).text();
    $('#suggestion').attr('value', currentSuggestion);
}

function confirm() {
    if (!currentRequest) {
        return;
    }
    var data = {
        id: currentRequest.id,
        location: {
            suggestion: $('#suggestion').val(),
            lat: $('#lat').val(),
            lng: $('#lng').val()
        }
    }
    socket.emit('identifier/submit', data, function() {
        requests.shift();
        $('#request-number').text((requests.length).toString());
        swal("Thông báo", "Đã gửi thông tin cho tài xế", "success");
        currentRequest = undefined;
        next();
    });
    return this;
}

$('#next').click(next);

$('.card').draggable({
    handle: "div.card-header"
});

addDraggableMarker(function(lat, lng) {
    $('#lat').attr('value', lat);
    $('#lng').attr('value', lng);
})