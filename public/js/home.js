var username = sessionStorage.getItem('username');

$('#username').text(username);
$(".dropdown-item#logout").on('click', function(event) {
    var param = {
        username: username
    };

    $.post('/logout', param)
        .done(function(result) {
            window.location.pathname = '/';
        })
        .fail(function(xhr, status, error) {
            swal("Oops!!", "Đã có vấn đề gì đó...", "error");
        });
});

$("#btn-datxe").on('click', function() {
    var name = $('#name').val();
    var phone = $('#phone').val();
    var address = $('#address').val();
    var note = $('#note').val();

    if (name.length == 0) {
        $('#name').css({ "border": " 2px solid #E5293C" });
        swal("Thông báo", "Vui lòng nhập đầy đủ thông tin khách hàng", "error");
    } else if (phone.length == 0) {
        $('#phone').css({ "border": " 2px solid #E5293C" });
        swal("Thông báo", "Vui lòng nhập đầy đủ thông tin khách hàng", "error");
    } else if (address.length == 0) {
        $('#address').css({ "border": " 2px solid #E5293C" });
        swal("Thông báo", "Vui lòng nhập đầy đủ thông tin khách hàng", "error");
    } else {
        var url = `http://localhost:8080/submit?name=${name}&phone=${phone}&address=${address}&note=${note}`;
        $.get(url, (id) => {
            $('#name').css({ "border": " 2px solid #0072FF" });
            $('#name').val("");
            $('#phone').css({ "border": " 2px solid #0072FF" });
            $('#phone').val("");
            $('#address').css({ "border": " 2px solid #0072FF" });
            $('#address').val("");
            $('#note').css({ "border": " 2px solid #0072FF" });
            $('#note').val("");
            swal("Thông báo", "Đã gửi thông tin khách hàng về hệ thống", "success");
        });
    }
});

$('#name').keypress(function(event) {
    if (event.which == 13) {
        jQuery("#btn-datxe").focus().click();
    }
});

$('#phone').keypress(function(event) {
    if (event.which == 13) {
        jQuery("#btn-datxe").focus().click();
    }
});

$('#address').keypress(function(event) {
    if (event.which == 13) {
        jQuery("#btn-datxe").focus().click();
    }
});

$('#note').keypress(function(event) {
    if (event.which == 13) {
        jQuery("#btn-datxe").focus().click();
    }
});