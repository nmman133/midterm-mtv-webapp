Handlebars.registerHelper('equal', function(lvalue, rvalue, options) {
    if (lvalue == rvalue) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
});

var requests = [];
var socket;
var username = sessionStorage.getItem("username");
var curReqId;
console.log(username);

socket = io.connect('http://localhost:8080');

socket.on('new-token', function(uname, acToken, reToken) {
    accessToken = acToken;
    refreshToken = reToken;
});

socket.on('connect', function() {
    console.log('connected');
});

socket.on('disconnect', function() {
    console.log('disconnected');
    window.location.pathname = '/';
});

socket.on('manager/update', function(data) {
    requests = data;
    updateUI();
});


function updateUI() {
    var source = $('#request-template').html();
    var template = Handlebars.compile(source);

    var html = template({
        requests: requests
    })

    $('#request-list').html(html);
    if ($('.detail-btn').length) {
        $('.detail-btn').click(onDetailBtnClick);
    }

    if (curReqId) {
        for (i in requests) {
            var req = requests[i];
            if (req.id == curReqId) {
                if (req.status == 4 || req.status == 5) {
                    $('#myModal').modal('hide');
                    break;
                }
                if (req.status == 3) {
                    removeRoute();
                    removeUser();
                }
                var source = $('#detail-template').html();
                var template = Handlebars.compile(source);
                var html = template(req);
                $('.modal-detail').html(html);
                updateDriver(req.driver.location.lat, req.driver.location.lng);
                break;
            }
        };
    }
}

function onDetailBtnClick() {
    var id = $(this).attr('id');
    for (i in requests) {
        var req = requests[i];
        if (req.id == id) {
            createModal(req);
            curReqId = id;
            break;
        }
    };
    $('#myModal').modal('show');
}

function createModal(request) {
    var source = $('#detail-template').html();
    var template = Handlebars.compile(source);
    var html = template(request);
    $('.modal-detail').html(html);
    $(document).ready(function() {
        $('#myModal').on('hidden.bs.modal', function() {
            curReqId = undefined;
        })
        var userLocation = {
            lat: request.location.lat,
            lng: request.location.lng
        }
        createMap(request.driver.location, userLocation);
    })
}

$('#username').text(username);
$(".dropdown-item#logout").on('click', function(event) {
    var param = {
        username: username
    };

    $.post('/logout', param)
        .done(function(result) {
            window.location.pathname = '/';
        })
        .fail(function(xhr, status, error) {
            swal("Oops!!", "Đã có vấn đề gì đó...", "error");

        });
});

updateUI();