$("#btn-dangnhap").on('click', function(event) {
    var param = {
        username: $('#username').val(),
        password: $('#password').val()
    };
    if (param.username.length == 0) {
        $('#username').css({ "border": " 2px solid #E5293C" });
    } else if (param.password.length == 0) {
        $('#password').css({ "border": " 2px solid #E5293C" });
    }

    if (param.username.length == 0 || param.password.length == 0) {
        $('#username').css({ "border": " 2px solid #E5293C" });
        $('#password').css({ "border": " 2px solid #E5293C" });
    } else {
        $('.modal').modal('show');
        setTimeout(function() {
            $.post('/logon', param)
                .done(function(result) {
                    sessionStorage.setItem('username', result.username);
                    window.location.pathname = result.next_url;
                    $('.modal').modal('hide');
                })
                .fail(function(xhr, status, error) {
                    $('.modal').modal('hide');
                    swal("Oops!!", "Đăng nhập không thành công?!", "error");
                });
        }, 1000);
    }
});

$('#password').keypress(function(event) {
    if (event.which == 13) {
        jQuery("#btn-dangnhap").focus().click();
    }
});

$('#username').keypress(function(event) {
    if (event.which == 13) {
        jQuery("#btn-dangnhap").focus().click();
    }
});