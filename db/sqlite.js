var sqlite = require('sqlite-sync');
var moment = require('moment');
var md5 = require('crypto-js/md5');

exports.connect = function(dbname) {
    //Connecting - if the file does not exist it will be created
    var db = sqlite.connect(dbname);
    // Check table DRIVER is or not exist
    var table_user = sqlite.run("SELECT name FROM sqlite_master WHERE type='table' AND name='USER'");
    // Create table USER
    if(table_user.length == 0) {
        sqlite.run("CREATE TABLE USER(ID  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, USERNAME TEXT UNIQUE, PASSWORD STRING, NAME TEXT, PHONE TEXT, ROLE INT)");
        // Insert data
        for(var i=0;i<10;i++){
            var user = i == 0 ? "admin" :"user"+i;
            var pass = i == 0 ? "admin" : "pass"+i;
            var name = i == 0 ? "admin" : "name"+i;
            var phone = i == 0 ? "00000" : "0999"+i;
            sqlite.insert("USER",{USERNAME: user, PASSWORD: md5(pass).toString(), NAME: name, PHONE: phone, ROLE: i == 0 ? 1 : 2});
        }
        for(var i=1;i<6;i++){
            var user = "submitter"+i;
            var pass = "pass"+i;
            var name = "submitter_name"+i;
            var phone = "0123"+i;
            sqlite.insert("USER",{USERNAME: user, PASSWORD: md5(pass).toString(), NAME: name, PHONE: phone, ROLE: 3});
        }
        for(var i=1;i<4;i++){
            var user = "identifier"+i;
            var pass = "pass"+i;
            var name = "identifier_name"+i;
            var phone = "0166"+i;
            sqlite.insert("USER",{USERNAME: user, PASSWORD: md5(pass).toString(), NAME: name, PHONE: phone, ROLE: 0});
        }
        // Show data after insert
        var allUser = this.getAllUser();
        for (var i = 0; i < allUser.length; i++) {
            user = allUser[i];
        }
    }

    // Check table USER is or not exist
    var table_token = sqlite.run("SELECT name FROM sqlite_master WHERE type='table' AND name='TOKEN'");
    // Create table TOKEN
    if(table_token.length == 0) {
        sqlite.run("CREATE TABLE TOKEN (USERNAME TEXT PRIMARY KEY NOT NULL UNIQUE, TOKEN TEXT, DATE DATE)");
    }
};


exports.insertUser = function(username, password, name, phone, role) {
    return sqlite.insert("USER",{USERNAME:user, PASSWORD:md5(pass).toString(), NAME: name, PHONE: phone, ROLE: role});
};

exports.updateToken = function(username, token) {
    return new Promise(function(resolve, reject) {
        sqlite.connect('db/PICKME.db');
        sqlite.run(`DELETE FROM TOKEN WHERE USERNAME = '${username}'`);
        sqlite.insert("TOKEN",{USERNAME: username, TOKEN: token, DATE: moment().unix()}, function(res){
            if (res.error) {
                reject(res.error);
            } else {
                resolve(res);
            }
            sqlite.close();
        });
    });
};

exports.getUser = function(username, password) {
    sqlite.connect('db/PICKME.db');
    return new Promise(function(resolve, reject) {
        sqlite.run("SELECT * FROM USER WHERE USERNAME ='"+ username + "' AND PASSWORD ='"+ md5(password).toString() + "'", function(res){
            if (res.error) {
                reject(res.error);
            } else {
                resolve(res);
            }
            sqlite.close();
        });
    });
};

exports.getUserByUserName = function(username) {
    sqlite.connect('db/PICKME.db');
    return new Promise(function(resolve, reject) {
        sqlite.run("SELECT * FROM USER WHERE USERNAME ='"+ username + "'", function(res){
            if (res.error) {
                reject(res.error);
            } else {
                resolve(res);
            }
            sqlite.close();
        });
    });
};

exports.getToken = function(username) {
    sqlite.connect('db/PICKME.db');
    return new Promise(function(resolve, reject) {
        sqlite.run("SELECT * FROM TOKEN WHERE USERNAME ='"+ username + "'", function(res){
            if (res.error) {
                reject(res.error);
            } else {
                resolve(res);
            }
            sqlite.close();
        });
    });
};

exports.deleteToken = function(username) {
    sqlite.connect('db/PICKME.db');
    sqlite.run(`DELETE FROM TOKEN WHERE USERNAME = '${username}'`, (res) => {
        sqlite.close();
    });
};
